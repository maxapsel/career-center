import java.util.Scanner;
public class TestExceptions {
    public static void main(String [] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("yes or no");
        String test = in.nextLine();
        if (!(test.equals("yes") || test.equals("no")))
            test = "no";
        try {
            System.out.println("start try"); 
            doRisky(test); 
            System.out.println("end try");
        } 
        catch (Exception se) { 
            System.out.println("exception");
        } 
        finally { 
            System.out.println("finally");
        }
        System.out.println("end of main"); 
    }
    static void doRisky(String test) throws Exception { 
        System.out.println("start risky");
        if ("yes".equals(test)) {
            throw new Exception(); 
        }
        System.out.println("end risky");
        return; 
    }
}