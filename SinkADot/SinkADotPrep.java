public class SinkADotPrep {
	private int[] locationCells;
	private int numOfHits = 0;

	public String checkYourself(String guessStr) {
		int guess = Integer.parseInt(guessStr);
		String result = "miss";
		for (int cells : locationCells) {
			if (guess == cells) {
				result = "hit";
				numOfHits++;
				break;
			}
		}
		if (numOfHits == locationCells.length)
		{
			result = "kill";
		}
		System.out.println(result);
		return result;
	}
	
	public void setLocationCells(int[] loc){
		locationCells = loc;
	}
}
