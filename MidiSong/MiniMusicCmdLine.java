import javax.sound.midi.*;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class MiniMusicCmdLine { //open class
    public static void main(String[] args) throws Exception, IOException, InvalidMidiDataException {//open main 
        MiniMusicCmdLine mini = new MiniMusicCmdLine();
        Scanner in = new Scanner(System.in);
        System.out.println("Are you the real Slim Shady? (1 for yes, 2 for no)");
        int slim = in.nextInt();
        if(slim==1){try
            {
                //open if
            mini.slimShady();
            }
            catch (MidiUnavailableException mue)
            {
                mue.printStackTrace();
            }
        }//close if
        else{//open else
            if(args.length < 2) {//open if
                System.out.println("Don’t forget the instrument and note args"); 
            } //close if
            else {//open else
                int instrument = Integer.parseInt(args[0]); 
                int note = Integer.parseInt(args[1]);
                for (int i = 0; i < 100; i++){
                    if (instrument - i > 1)
                        instrument = instrument - i;
                    else
                        instrument = instrument + 1;
                    
                    if (note + i < 100)
                        note = note + i;
                    else
                        note = note - 1;
                     
                    mini.play(instrument, note);
                    try
                    {
                        TimeUnit.MILLISECONDS.sleep(366);
                    }
                    catch (InterruptedException ie)
                    {
                        ie.printStackTrace();
                    }
                }
            }//close else
        }//close else
        System.exit(0);
    } // close main
    public void play(int instrument, int note) { //open play
        try {//open try
            Sequencer player = MidiSystem.getSequencer(); 
            player.open();
            Sequence seq = new Sequence(Sequence.PPQ, 4); 
            Track track = seq.createTrack();
            MidiEvent event = null;
            ShortMessage first = new ShortMessage(); 
            first.setMessage(192, 1, instrument, 0);
            MidiEvent changeInstrument = new MidiEvent(first, 1); 
            track.add(changeInstrument);
            ShortMessage a = new ShortMessage(); 
            a.setMessage(144, 1, note, 100); 
            MidiEvent noteOn = new MidiEvent(a, 1); 
            track.add(noteOn);
            ShortMessage b = new ShortMessage(); 
            b.setMessage(128, 1, note, 100); 
            MidiEvent noteOff = new MidiEvent(b, 16); 
            track.add(noteOff); 
            player.setSequence(seq);
            player.start();
        } //close try
        catch (Exception ex) {ex.printStackTrace();} 
    } // close play
    public void slimShady() throws InvalidMidiDataException, IOException, MidiUnavailableException, Exception{//open Em
        try{
            // Create a sequencer for the sequence
            Sequencer sequencer = MidiSystem.getSequencer();
            sequencer.open();
            InputStream is = new BufferedInputStream(new FileInputStream(new File("LoseYourself.mid")));
            sequencer.setSequence(is);
        
            // Start playing
            sequencer.start();
        }
        catch (Exception ex) {ex.printStackTrace();}
    }//close Em
} // close class